<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Requests
 *
 * @author Roman Rabirokh
 */
class Requests {
    //put your code here
    private $curl;
    private $site;
    private $to;
    
    public  function  __construct($id)
    {
        $this->curl= curl_init();
        $this->site=$id;// id site
        $this->to="sec.dg-star.com";
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $this->to,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query(array('id' => $this->site))
        ));
    }
    public function processRequest()
    {
        $response = curl_exec($this->curl);
        curl_close($this->curl);
        return $response;
    }
}
