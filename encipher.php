<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of encipher
 *
 * @author Roman Rabirokh
 */
class Encipher {
    //put your code here
    private $publicKey;
    private $privateKey;
    private $isBase64Coding;
    private $lenght;
    public function __construct($publicKey=false,$privateKey=false,$base64=true)
    {
        $this->privateKey=$privateKey ? $privateKey : false;
        $this->publicKey=$publicKey ? $publicKey : false;
        $this->isBase64Coding=$base64;
    }
    public function encode($text)
    {
        if($this->publicKey)
        {
            $this->setLenghtPub();
            $pb  = openssl_pkey_get_public($this->publicKey);
            $output = '';
            while ($text)
            {
                $chunk = substr($text, 0, $this->lenght);
                $text = substr($text, $this->lenght);
                $encrypted = '';
                openssl_public_encrypt($chunk, $encrypted, $pb);
                $output .= $encrypted;
            }
            if($this->isBase64Coding)
            {
                $encrypted=chunk_split(base64_encode($output));
            }
            return $encrypted;
        }
        else
        {
            return false;
        }
    }
    public function decode($encriptText)
    {
        if($this->privateKey)
        {
            $this->setLenghtPk();
            $pk  = openssl_pkey_get_private($this->privateKey);
            if($this->isBase64Coding)
            {
                $encriptText=base64_decode($encriptText);
            }
            $text = '';
            while ($encriptText)
            {
                $chunk = substr($encriptText, 0, $this->lenght);
                $encriptText = substr($encriptText, $this->lenght);
                $decrypted = '';
                openssl_private_decrypt($chunk, $decrypted, $pk);
                $text .= $decrypted;
            }
            return $text;
        }
        else 
        {
            return false;
        }
    }
    private function setLenghtPub()
    {
        $publicKey = openssl_pkey_get_public($this->publicKey);
        $a_key = openssl_pkey_get_details($publicKey);
        $this->lenght = ceil($a_key['bits'] / 8) - 11;
    }
    private function setLenghtPk()
    {
        $privateKey = openssl_pkey_get_private($this->privateKey);
        $a_key = openssl_pkey_get_details($privateKey);
        $this->lenght = ceil($a_key['bits'] / 8);

    }
}
