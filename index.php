<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include 'encipher.php';
include 'crypt.php';
include 'requests.php';

$key_pub='-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALjKghOz1HK2Vzm7Y6uiw+VheXoEVXtS
7bEwN7RG5C01af0H0sYkLMs18+349n3s6+SW4huFIq4Nda2L0p7wY00CAwEAAQ==
-----END PUBLIC KEY-----';

$key_pr= '-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBALjKghOz1HK2Vzm7Y6uiw+VheXoEVXtS7bEwN7RG5C01af0H0sYk
LMs18+349n3s6+SW4huFIq4Nda2L0p7wY00CAwEAAQJAEgxtcgExcdbPZCsiTARE
v+WqWLmNo9LwtkqwViJKbQPM4U60Y2wMPsmhAl4FrMhK7AjcMH2fJ/ycX4Ns6d31
nQIhAO5GVw3PaWB3M7rTlIle38p/afZw+OWeiGPcacPoQQ1zAiEAxomiWAQHuC2K
K4Ff/DoMYeRdQ5kytOXGkKHUF7NUnD8CIQCcdNGKvuuprPvprC53eTWniLxyVsII
PG3AbKWCPwogIwIhAK4J9QBG+CVh9l9Q2D2Y/UXLFF6vqx2c10XuNqhYEfGtAiAY
bdG88ol7Qr1surmlRUFIsKItEvD+VMS1LEgu2ZuwwQ==
-----END RSA PRIVATE KEY-----';

$enc= new Encipher($key_pub,$key_pr);
$files= new Crypt($enc);
$request= new Requests(1);

if($request->processRequest()==1)
{
    $files->proccesCrypt('encrypt');
    die;
}
